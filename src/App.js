import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import NaviBar from './components/NaviBar';
import Register from './pages/register';
import Login from './pages/Login';
import Home from './pages/Home';
import Logout from './pages/Logout';
import Catalog from './pages/Catalog';
import ItemView from './pages/ItemView';
import AddItem from './pages/AddItem';
import EditItem from './pages/EditItem';
import { UserProvider } from './UserContext';
import './App.css';

function App() {

  const [user, setUser] = useState({
      email: null,
      isAdmin: null,
      id: null
  })

  const unsetUser = () => {
    localStorage.clear();

    setUser({
      email: null,
      isAdmin: null,
      id: null

    })
  }

  useEffect(() => {

    setUser({

      // Initialized as an object with properties from localStorage
      email: localStorage.getItem('email'),

      // Added a condition to convert the string data type into boolean
      isAdmin: localStorage.getItem('isAdmin') === 'true',
      id: localStorage.getItem('id')

    })

  }, [])

  return (
    <React.Fragment>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <NaviBar />
            <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/login" component={Login}/>
                    <Route exact path="/register" component={Register}/>
                    <Route exact path="/logout" component={Logout} />
                    <Route exact path="/itemView/:itemId" component={ItemView}/>
                    <Route exact path="/catalog" component={Catalog} />
                    <Route exact path="/editItem/:itemId" component={EditItem}/>
                    <Route exact path="/addItem" component={AddItem} />
            </Switch>
        </Router>
      </UserProvider>
    </React.Fragment>
  );
}

export default App;
