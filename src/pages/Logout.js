import React, {useContext, useEffect} from 'react';
import {Redirect} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout (){
	const { unsetUser } = useContext(UserContext);

	//clear the local storage of the user's information'
	unsetUser();

	//invoke unsetUser only aftr initial render
	useEffect(() => {
		unsetUser();
	}, [unsetUser])
	
	//Redirect back to login
	return(
		<Redirect to='/login'/>
	)
}