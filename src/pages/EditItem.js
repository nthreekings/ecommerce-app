import React,{ useState,useEffect } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import {Redirect} from 'react-router-dom';
import Swal from 'sweetalert2';
import {useParams} from 'react-router-dom';

export default function EditItem(){

	const {_id} = useParams();

	const [name, setName] = useState('');
	const [price, setPrice] = useState(0);
	const [description, setDescription] = useState('');
	const [isActive, setIsActive] = useState(false);
	const [willRedirect, setWillRedirect] = useState(false)

	function editItemFetch(e){
		e.preventDefault();

		fetch(`https://thawing-woodland-85168.herokuapp.com/api/items/${_id}`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				price: price,
				description: description
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data){

				Swal.fire({
					// 'success',
					// 'Item edited',
					// 'Thank you'
					icon: 'success',
					title: 'Item Edited',
					text: 'Item edited successfully.'

				})
			}

			setWillRedirect(true);
		})
	}

	useEffect(() => {
		if ((name !== '') && (price !== '') && (description !== '')){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	},[name, price,description])

	return (

		
		willRedirect === true ? <Redirect to = '/catalog'/>
        :
        <div className="form-section">
        	<Container>
        		<div className="a-e-form" > 			
			        <h1 className="text-center mb-5">Edit Item</h1>
					<Form onSubmit = {e => editItemFetch(e)}>
						<Form.Group controlId="name">
							<Form.Label>Name</Form.Label>
							<Form.Control 
								type="text"
								placeholder = "Enter Item Name"
								value = {name}
								onChange = {(e) => setName(e.target.value)}
								required
								/>
						</Form.Group>

						<Form.Group controlId="description" className="mt-3">
							<Form.Label>Description</Form.Label>
							<Form.Control 
								type="text"
								as="textarea"
								rows={2}
								placeholder = "Enter Description"
								value = {description}
								onChange = {(e) => setDescription(e.target.value)}
								required
								/>
						</Form.Group>

						<Form.Group controlId="price" className="mt-3">
							<Form.Label>Price</Form.Label>
							<Form.Control 
								type="number"
								value = {price}
								onChange = {(e) => setPrice(e.target.value)}
								required
								/>
						</Form.Group>

						{ isActive ? 
							<Button type="submit" id="submitBtn" className="primary-button mt-4 d-block">Save Changes</Button>
							:
							<Button type="submit" id="submitBtn" className="secondary-button mt-4 d-block" disabled>Save Changes</Button>
						}
					</Form>
				</div>
			</Container>
		</div>
	)
}