import React, { useState, useEffect } from 'react';
import CardItem from '../components/CardItem';
import { Container, Row, Col, Button, Form } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import * as RiIcons from "react-icons/ri";
import { GiDonut } from "react-icons/gi";

export default function Home() {

	const [itemList, setItemList] = useState([]);

	useEffect(() => {

		fetch('https://thawing-woodland-85168.herokuapp.com/api/items/')
		.then(res => res.json())
		.then(data => {

			setItemList(data.map(perItem => {

				return(
					<Col md="4" className="mb-4">
						<CardItem key={perItem._id} item={perItem}/>
					</Col>
				)
			}).splice(0,6))

		})
	},[])

	return(
		<React.Fragment>
			<div className="landing" id="homepage">
				<div className="landing-section d-flex">
					<Container className="hero text-center mx-auto">
						<div className="containerbackground">
				            DONUTS
				        </div>
						
						<div className="hero-img-section">
					        <img
						      className="hero-img d-block mx-auto img-fluid"
						      src={require('../images/pink-donut-head.png').default}
						      alt="Donut"
						    />
						</div>
					</Container>
				</div>

				<div className="d-flex justify-content-center">
					<Link to="/catalog"><Button variant="info" block size="lg" className="btn cta-button text-white mx-auto my-4">Order Now</Button></Link>
					
				</div>
			</div>
			
			<div className="how-to-section">
				<h1 className="text-center">How to Order?</h1>
				<Container className="text-center my-5">
					<Row>
						<Col md="3" className="how-to-card my-3">
							<span><RiIcons.RiCursorFill className="steps-icon"/></span>
							<h5>Select Bundle</h5>
							<p>Select the bundle you like from our options</p>
						</Col>

						<Col md="3" className="how-to-card my-3">
							<span><GiDonut className="steps-icon"/></span>
							<h5>Customize Order</h5>
							<p>Select the flavor of your donuts</p>
						</Col>

						<Col md="3" className="how-to-card my-3">
							<span><RiIcons.RiBankCardFill className="steps-icon"/></span>
							<h5>Fill-out Payment Form</h5>
							<p>Input your information and payment details</p>
						</Col>

						<Col md="3" className="how-to-card my-3">
							<span><RiIcons.RiEBike2Fill className="steps-icon"/></span>
							<h5>Wait for Order</h5>
							<p>Wait until your donuts come to your doorsteps</p>
						</Col>
					</Row>
				</Container>
			</div>

			<div className="our-menu-section" id="ourmenu">
				<h1 className="text-center">Our Menu</h1>
				<Container className="text-center my-5">
					<Row>
						{itemList}			
					</Row>

					<Link to="/catalog"><Button block size="lg" className="btn primary-button-outline mx-auto my-4">View All</Button></Link>
					
				</Container>
			</div>

			<div className="about-us-section my-5" id="about">
				<Container>
					<Row className="d-flex align-items-center">
						<Col md="6">
							<div className="about-text">
								<h1>About Us</h1>
								<p className="mt-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
							</div>
						</Col>

						<Col md="6">
							<img
						      className="d-block img-fluid"
						      src={require('../images/donuts.png').default}
						      alt="Donut"
						    />
						</Col>
					</Row>
				</Container>
			</div>
			
			<div className="contact-us-section my-5" id="contact">
				<Container className="contact-us">
					<div className="contact-header">
						<h1 className="text-center">Contact Us</h1>
						<p className="text-center">Feel like contacting us? Reach us on any of our socials here and tell us your queries and we will get back to you as soon as possible.
						</p>
					</div>

					<Row className="contact-body py-5">
						<Col md={{ span: 3, offset: 2 }}>
							<Row>
								<p className="mb-0"><span><RiIcons.RiTimeFill className="contact-icon"/></span>Opening Hours</p>
								<ul>
									<li>Monday-Saturday</li>
									<li>10am-7pm</li>
								</ul>
							</Row>

							<Row>
								<p className="mb-0"><span><RiIcons.RiMapPinFill className="contact-icon"/></span>Address</p>
								<ul>
									<li>Tirona Hi-way, Habay II Bacoor, Cavite, Philippines</li>
								</ul>
							</Row>
							
							<Row>
								<p className="mb-0"><span><RiIcons.RiPhoneFill  className="contact-icon"/></span>Support</p>
								<ul>
									<li>hello@mail.com</li>
									<li>(02)529-8961</li>
								</ul>
							</Row>
						</Col>

						<Col md="5">
							<div className="contact-form">
								<Form>
								  <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
								    <Form.Label>Full Name</Form.Label>
								    <Form.Control type="text" placeholder="Full Name" />
								  </Form.Group>

								  <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
								    <Form.Label>Email address</Form.Label>
								    <Form.Control type="email" placeholder="name@example.com" />
								  </Form.Group>

								  <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
								    <Form.Label>Message</Form.Label>
								    <Form.Control as="textarea" rows={3} />
								  </Form.Group>

								  <Button variant="info" type="submit" className="cta-button text-white">Submit</Button>
								</Form>
							</div>
						</Col>
					</Row>				
				</Container>
			</div>
		</React.Fragment>
	)
}