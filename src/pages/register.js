import React,{ useState,useEffect } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import AppHelper from '../app-helper'
import {Redirect} from 'react-router-dom';



export default function Register(){
	const [firstName,setFirstName] = useState("");
	const [lastName,setLastName] = useState("");
	const [mobileNo,setMobileNo] = useState(0);
	const [email,setEmail] = useState("");
	const [password,setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);
	const [willRedirect, setWillRedirect] = useState(false)


	function registerUser(e) {
		e.preventDefault();

		fetch(`${AppHelper.API_URL}/users/email-exists/`,{

			method: 'POST',
			headers: {

				'Content-Type': 'application/json'

			},
			body: JSON.stringify({

				email: email

			})

		})
		.then(res => res.json()) 
		.then(data => {

			
			if(data === false){

				
				fetch(`${AppHelper.API_URL}/users/`,{

					method: 'POST',
					headers: {
						'Content-Type': 'application/json'

					},
					body: JSON.stringify({
						firstname: firstName,
						lastname: lastName,
						mobileNo: mobileNo,
						password: password,
						email: email
						
					})

				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if(data === true){

						Swal.fire({

							icon: 'success',
							title: 'Registration Successful.',
							text: 'Thank you for registering.'

						})

						setWillRedirect(true)


					} else {

		
						Swal.fire({

							icon: 'error',
							title: 'User Registration Failed.',
							text: 'An internal server error has occured.'

						})

					}

				})

			} else {
				Swal.fire({

					icon: 'error',
					title: 'Registration Failed.',
					text: 'Email has already been registered.'

				})

			}


		})

		setFirstName("")
		setLastName("")
		setMobileNo(0)
		setEmail("")
		setPassword("")
		

	}

	useEffect(() => {

		if((firstName !=='' && lastName !== '' && mobileNo !== 0 && email !== '' && password !== '' ) && (mobileNo.length === 11)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [firstName,lastName,mobileNo,email, password]);

	return (
		willRedirect === true ? <Redirect to = '/login'/>
        :
		<div className="register-section">
		<Container>
			<div className="register-form">
				<h1 className="text-center mb-5">Register</h1>
				<Form onSubmit={(e) => registerUser(e)}>
					<Row>
						<Col md="6">
							<Form.Group controlId="firstName">
								<Form.Label>First Name</Form.Label>
								<Form.Control 
									type="text"
									placeholder="Enter First Name"
									value={firstName}
									onChange={(e) => setFirstName(e.target.value)}
									required
								/>
							</Form.Group>
						</Col>

						<Col md="6">
							<Form.Group controlId="lastName">
								<Form.Label>Last Name</Form.Label>
								<Form.Control 
									type="text"
									placeholder="Enter Last Name"
									value={lastName}
									onChange={(e) => setLastName(e.target.value)}
									required
								/>
							</Form.Group>
						</Col>
					
						<Col md="12">
							<Form.Group controlId="mobileNo" className="mt-3">
								<Form.Label>Mobile Number</Form.Label>
								<Form.Control 
									type="number"
									value={mobileNo}
									onChange={(e) => setMobileNo(e.target.value)}
									required
								/>
							</Form.Group>
						</Col>

						<Col md="12">
							<Form.Group controlId="userEmail" className="mt-3">
								<Form.Label>Email Address</Form.Label>
								<Form.Control 
									type="email"
									placeholder="Enter email"
									value={email}
									onChange={(e) => setEmail(e.target.value)}
									required
								/>
							</Form.Group>
						</Col>

						<Col md="12">
							<Form.Group controlId="password" className="mt-3">
								<Form.Label>Password</Form.Label>
								<Form.Control 
									type="password"
									placeholder="Password"
									value={password}
									onChange={(e) => setPassword(e.target.value)}
									required
								/>
							</Form.Group>
						</Col>
					</Row>
					{ isActive ? 
						<div className="d-grid gap-2">
						    <Button variant="primary" type="submit" id="submitBtn" className="primary-button mt-4 d-block">
						        Submit
						    </Button>
						</div>
						:
						<div className="d-grid gap-2">
                            <Button variant="danger" type="submit" id="submitBtn" className="secondary-button mt-4 d-block" disabled>
                                Submit
                            </Button>
                        </div>
					}
					
				</Form>
			</div>
		</Container>
	</div>
	)

}
