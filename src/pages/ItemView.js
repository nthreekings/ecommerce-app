import React, {Fragment} from 'react';
import {useParams} from 'react-router-dom';
// import {Card, Button} from 'react-bootstrap'
import Item from '../components/Item'

export default function ItemView(){

	const [viewItem, setViewItem] = ([]);

	const {itemId} = useParams();

	fetch(`https://thawing-woodland-85168.herokuapp.com/api/items/${itemId}`, {
		headers: {
            'Authorization': `Bearer ${localStorage.getItem('token')}` 
        }
	})
	.then(res => res.json())
	.then(data => {

		console.log(data)

		const viewItem = <Item key={data._id} item={data}/>

		setViewItem(viewItem)

	})

	return(
			<Fragment>
				{viewItem}
			</Fragment>

		)

}