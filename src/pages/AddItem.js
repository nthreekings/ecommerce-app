import React,{ useState,useEffect } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import {Redirect} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function AddItem(){

	const [name, setName] = useState('');
	const [price, setPrice] = useState(0);
	const [description, setDescription] = useState('');
	const [isActive, setIsActive] = useState(false);
	const [willRedirect, setWillRedirect] = useState(false)

	const token = localStorage.getItem('token');

	function addItemFetch(e){
		e.preventDefault();

		fetch('https://thawing-woodland-85168.herokuapp.com/api/items/',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				price: price,
				description: description
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data){

				Swal.fire({
					// 'success',
					// 'Item added',
					// 'Thank you'
					icon: 'success',
							title: 'Item Added',
							text: 'Item added successfully.'

				})
			}
			
			setWillRedirect(true)
			
		})
	}

	useEffect(() => {
		if ((name !== '') && (price !== '') && (description !== '')){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	},[name, price,description])

	return (
		
		willRedirect === true ? <Redirect to = '/catalog'/>
        :
		<div className="form-section">
    		<Container>
    			<div className="a-e-form">
		        <h1 className="text-center mb-5">Add Item</h1>
				<Form onSubmit = {e => addItemFetch(e)}>
					<Form.Group controlId="name">
						<Form.Label>Name</Form.Label>
						<Form.Control 
							type="text"
							placeholder = "Enter Item Name"
							value = {name}
							onChange = {(e) => setName(e.target.value)}
							required
							/>
					</Form.Group>

					<Form.Group controlId="description" className="mt-3">
						<Form.Label>Description</Form.Label>
						<Form.Control 
							type="text"
							as="textarea"
							rows={2}
							placeholder = "Enter Description"
							value = {description}
							onChange = {(e) => setDescription(e.target.value)}
							required
							/>
					</Form.Group>

					<Form.Group controlId="price" className="mt-3">
						<Form.Label>Price</Form.Label>
						<Form.Control 
							type="number"
							value = {price}
							onChange = {(e) => setPrice(e.target.value)}
							required
							/>
					</Form.Group>

					{ isActive ? 
						<Button type="submit" id="submitBtn" className="primary-button mt-4 d-block">Add Item</Button>
						:
						<Button type="submit" id="submitBtn" className="secondary-button mt-4 d-block" disabled>Add Item</Button>
					}
				</Form>
			</div>
		</Container>
		</div>
	)
}