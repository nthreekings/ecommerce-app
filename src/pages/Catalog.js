import React, {Fragment, useContext, useState, useEffect } from 'react';
import CardItem from '../components/CardItem';
import { Container, Col, Row, Table, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import {Link} from 'react-router-dom';

export default function Catalog(){

	const {user} = useContext(UserContext)
	const [deleteItem, setDeleteItem] = useState('');
	const [itemList, setItemList] = useState([]);
	const [adminItemList, setAdminItemList] = useState([]);

	//for archiving item
	useEffect(() => {

		fetch(`https://thawing-woodland-85168.herokuapp.com/api/items/${deleteItem}`)
		.then(res => res.json())
		.then(data => {

			if(data){
				alert('Item deleted')
			}
		})
	},[deleteItem])

	useEffect(() => {
		//////////////////For admin
		fetch('https://thawing-woodland-85168.herokuapp.com/api/items/', {
			headers: {
	            'Authorization': `Bearer ${localStorage.getItem('token')}`
	        }
		})
		.then(res => res.json())
		.then(data => {
			
			setAdminItemList(data.map(perItem => {
				return(
					<tr key={perItem._id}>
					<td>{perItem.name}</td>
					<td>{perItem.description}</td>
					<td>{perItem.price}</td>
					<td>
						<Link to={`/editItem/${perItem._id}`}><Button className="bg-warning">Edit</Button></Link>
						&nbsp;
	                    <Button className="bg-danger" value={perItem._id} onClick={(e) => setDeleteItem(e.target.value)}>Delete</Button>
                    </td>
				</tr>

				)
			}))
		})
	}, [])

	useEffect(() => {

		fetch('https://thawing-woodland-85168.herokuapp.com/api/items/')
		.then(res => res.json())
		.then(data => {

			setItemList(data.map(perItem => {

				return(
					<Col md="4" className="mb-4">
						<CardItem key={perItem._id} item = {perItem}/>
					</Col>
				)
			}))

		})
	}, [])

	if (user.isAdmin === true ){

		return (
			// if admin
			<Fragment>
				<Container>
					<div className="mt-5 d-flex justify-content-between">
						<span><h1>Item List</h1></span>
						<Link to="/addItem"><Button variant="info" className="text-white">Add Item</Button></Link>
					</div>
					
					<Table striped bordered hover>
						<thead>
							<tr>
								<th>Name</th>
								<th>Description</th>
								<th>Price</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							{adminItemList}
						</tbody>
					</Table>
				</Container>
			</Fragment>
		)
	} else {
		// if user only
		return(
			<Fragment>
				<Container>
					<h1 className="mt-5 text-center">Our Menu</h1>
					<Row className="mt-5">
						{itemList}
					</Row>
				</Container>
			</Fragment>
		)
	}
}
