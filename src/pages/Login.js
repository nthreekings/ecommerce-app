import React, { useState, useEffect, useContext } from 'react';
import {Redirect} from 'react-router-dom'
import { Container, Form, Button } from 'react-bootstrap';
// import AppHelper from '../app-helper';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login() {

    const {setUser} = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);
    const [willRedirect, setWillRedirect] = useState(false);

    function authenticate(e) {

        e.preventDefault();

        fetch(`https://thawing-woodland-85168.herokuapp.com/api/users/login`,{
            method: 'POST',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {

            if(typeof data.access !== 'undefined'){
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);
            } else {
                Swal.fire('Authentication Failed', 'Something is wrong', 'error')
            }
        })
    }

    function retrieveUserDetails(access){
        fetch('https://thawing-woodland-85168.herokuapp.com/api/users/details',{
            headers: {
                Authorization: `Bearer ${access}`
            }
        })
        .then(res => res.json())
        .then(data => {
            
            setUser({
                id:data._id,
                isAdmin: data.isAdmin,
                email: data.email

            })

            let order = {
                userId: data._id,
                total: 0,
                items:[{}]
            }

            localStorage.setItem('orders', order)

            setWillRedirect(true)
        })
    }

    useEffect(() => {

        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    return (
        willRedirect === true ? <Redirect to = '/'/>
        :
        <div className="login-section">
            <Container>
                <div className="login-form">
                    <h1 className="text-center mb-5">Login</h1>
                    <Form onSubmit={(e) => authenticate(e)}>
                        <Form.Group controlId="userEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                                type="email" 
                                placeholder="Enter email"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="password" className="mt-3">
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                                type="password" 
                                placeholder="Password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                                required
                            />
                        </Form.Group>

                        {isActive ? 
                            <div className="d-grid gap-2">
                                <Button variant="primary" type="submit" id="submitBtn" className="primary-button mt-4 d-block">
                                    Submit
                                </Button>
                            </div>
                            : 
                            <div className="d-grid gap-2">
                                <Button variant="danger" type="submit" id="submitBtn" className="secondary-button mt-4 d-block" disabled>
                                    Submit
                                </Button>
                            </div>
                        }

                    </Form>
                </div>
            </Container>
        </div>
    )

}
