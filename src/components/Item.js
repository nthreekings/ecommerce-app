import React, {useState} from 'react';
// import {useParams} from 'react-router-dom';
import {Card, Button, Form} from 'react-bootstrap'
import {Redirect} from 'react-router-dom';

export default function ItemView({item}){
	
	const {name, description, price, _id} = {item}

	const [quantity, setQuantity] = useState(0)

	function addToCart(e){

		e.preventDefault();

		let subtotal = quantity*price
		let total = localStorage.order.getItem('total');
		total += subtotal

		localStorage.order.put({total: total})

		localStorage.order.items.put({
			 itemId: `${_id}`,
			 quantity: `${quantity}`,
			 subtotal: `${subtotal}`
		})

		return(
			<Redirect to='/cart' />
		)

	}

		return(
			<React.Fragment>
			<Card>
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description: </Card.Subtitle>
					<Card.Text>{description}</Card.Text>

					<Card.Subtitle>Price: </Card.Subtitle>
					<Card.Text>Php {price}</Card.Text>
					<br />
					<Form onSubmit={e => addToCart(e)}>
						<Form.Group>
							<Form.Control 
							type="number"
							placeholder="Quantity"
							value={quantity}
							onChange = {(e) => setQuantity(e.target.value)}
							required
							/>
						</Form.Group>
					</Form>

					<Button variant="info" className="text-white" type="submit">Add to Cart</Button>
				</Card.Body>
			</Card>
			</React.Fragment>
		)



}