import React, { useState, useEffect, useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavDropdown} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import * as RiIcons from "react-icons/ri";

export default function NaviBar() {

	const [scrollBar, setscrollBar] = useState(false);

    const changeColor = () => {
        if(window.scrollY >= 40) {
            setscrollBar(true)
        } else {
            setscrollBar(false)
        }
    };

    useEffect(() => {
        window.addEventListener('scroll', changeColor);
    }, [])

	const { user } = useContext(UserContext);

	return (
		<Navbar bg="light" expand="lg" sticky="top" className={scrollBar ? 'px-5 active-scroll' : 'px-5'}>
		  <Navbar.Brand as={Link} to="/">DONUTS</Navbar.Brand>
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="d-flex justify-content-between w-100">
			    <div className="nav-items">
					<Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
					<Nav.Link as={NavLink} to="/catalog" exact>Our Menu</Nav.Link>
					<Nav.Link as={NavLink} to="/" exact>About Us</Nav.Link>
					<Nav.Link as={NavLink} to="/" exact>Contact Us</Nav.Link>
		      	</div>

		      	<div className="nav-items">
		      		{(user.email !== null) ?
				      	<React.Fragment>
					      	<NavDropdown title="Account" id="nav-dropdown">
								<NavDropdown.Item as={NavLink} to="/logout">Logout</NavDropdown.Item>
							</NavDropdown>
							<Nav.Link as={NavLink} to="/" exact><span><RiIcons.RiShoppingCartLine /></span></Nav.Link>
				      	</React.Fragment>
				      	:
				      	<React.Fragment>
				      		<Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
				      		<Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
				      	</React.Fragment>
			      	}
		      	</div>
		      
		    </Nav>
		  </Navbar.Collapse>
		</Navbar>
	)
}
