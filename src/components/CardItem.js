import React from 'react';
import { Card, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CardItem({item}){

	// const {name, price, _id} = item;]

	console.log(item._id)

	return(
		<React.Fragment>
			<Card className="h-100">
			  <Card.Img variant="top" src="holder.js/100px180" />
			  <Card.Body>
			    <Card.Title>{item.name}</Card.Title>
			    <Card.Text>
			   		<span className="subtitle">Price: </span>
                    {item.price}

                    <p>{item.description}</p>
			    </Card.Text>
			    <Link to={`/itemView/${item._id}`}><Button variant="info" className="secondary-button text-white">View Product</Button></Link>  
			  </Card.Body>
			</Card>
		</React.Fragment>
	)

}